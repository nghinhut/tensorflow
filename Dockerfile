ARG TENSORFLOW_IMAGE_TAG

FROM tensorflow/tensorflow:$TENSORFLOW_IMAGE_TAG

## Update Image
RUN export DEBIAN_FRONTEND=noninteractive && apt-get update


## TFX Required packages
RUN apt-get install -y \
    build-essential libssl-dev libffi-dev \
    libxml2-dev libxslt1-dev zlib1g-dev \
    python3-pip git software-properties-common

RUN export DEBIAN_FRONTEND=noninteractive && apt-get install -y git vim nmap vagrant \
    && pip3 install numba \
    && curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh \
    && apt-get install -y software-properties-common && apt-add-repository --yes --update ppa:ansible/ansible && apt-get install -y ansible
